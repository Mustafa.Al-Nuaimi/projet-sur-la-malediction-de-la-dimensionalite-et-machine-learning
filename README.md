# Projet sur la Malédiction de la Dimensionalité et Machine Learning

*Personne responsable: Stephane Marchand-Maillet *

*Encadrant: François Fleuret *

*Étudiants: Merahi Reda, Ben Salem Fehmi, Mustafa Al-Nuaimi *

## Consignes

La dimension des données est un paramètre important influant les performances
de l’Apprentissage Statistique (Machine Learning). Le but de ce projet est
de produire des démonstrateurs (par exemple avec Flask+Python) illustrant
la Malédiction/Bénéfice de la Dimensionalié (Curse/Blessing of Dimension) à
partir de données aleatoires ou connues.

Exemple:

• https://stats.stackexchange.com/questions/451027/mathematicaldemonstration-of-the-distance-concentration-in-high-dimensions

Refs:

• https://flask.palletsprojects.com/en/3.0.x/quickstart/#a-minimalapplication

• https://arxiv.org/abs/cs/0703125

• https://en.wikipedia.org/wiki/Curse_of_dimensionality

## Description du projet

Le projet devra:

• creer des scripts Python pour des demonstrateurs (voir exemple ci dessous)

• Intégrer ces demonstrateurs dans le site Flask http://learn-ml.unige.ch/

• Le code devra être depose sur GitLab

## Exemples de demonstrateurs:

• Concentration des distances

• Projection d un hypercube

• Hypercube versus hypersphere

• ...

document.addEventListener('DOMContentLoaded', function() {
    const links = document.querySelectorAll('nav.nav a');

    links.forEach(link => {
        link.addEventListener('click', function(e) {
            e.preventDefault();

            const targetId = this.getAttribute('href').substring(1);
            const targetSection = document.getElementById(targetId);

            if (targetSection) {
                targetSection.scrollIntoView({
                    behavior: 'smooth',
                    block: 'start'
                });
            }
        });
    });
});

function switchLang() {
    var selectedLang = this.value;
    // Redirige vers la page correspondante selon la langue sélectionnée
    if (selectedLang === 'en') {
        window.location.href = 'page-en.html'; // Page en anglais
    } else if (selectedLang === 'fr') {
        window.location.href = 'page-fr.html'; // Page en français
    }
}

document.getElementById('langSelect').addEventListener('change', switchLang);


// Fonction JS pour basculer entre les thèmes
function switchTheme() {
    const themeStylesheet = document.getElementById('themeStylesheet');
    if (themeStylesheet.getAttribute('href') === '../Css/light-theme.css') {
        themeStylesheet.setAttribute('href', '../Css/dark-theme.css');
    } else {
        themeStylesheet.setAttribute('href', '../Css/light-theme.css');
    }
}

// Écouteur d'événements
document.getElementById('themeSwitch').addEventListener('click', switchTheme);

